# Final-TeVeO

# ENTREGA CONVOCATORIA JUNIO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Alejandro De Maeseneire Martinez
* Titulación: Ingenieria Telematica
* Cuenta en laboratorios: alexdmm
* Cuenta URJC: a.demaeseneire.2020@alumnos.urjc.es
* Video básico (url): https://youtu.be/7ng_KqLVcmI
* Video parte opcional (url): https://youtu.be/9ydRWnLbuws
* Despliegue (url):  https://finalteveo24.pythonanywhere.com/teveo/
* Contraseñas:
* Cuenta Admin Site: admin/admin

## Resumen parte obligatoria
Tenemos una pagina principal (recurso principal) en el cual podemos ver una barra de menu con un boton con el logo de la practicas /TeVeO (puedes volver a la pagina princpal) y distintas pestañas para acceder al resto de recursos de la pagina web. Debajo veremos un listado por paginas de 5 comentarios cada una, los comentarios estaran ordenados de mas nuevo a mas antiguo y por ultimo un pie de la pagina con un resumen del numero de Camaras y numero de comentarios. 
En la pagina de las camaras podremos darle al boton de descargar cualquiera de los dos listados que tenemos, los cuales se descargan directamente al darle, apareceran todas las camaras disponibles (con su identificador, un enlace a la pagina de la camara, enlace a la pagina dinamica de la pagina, nombre y numero de comentarios). Si pulsamos en el enlace de caulquiera de las paginas de la camara, nos llevara ala recurso en el cual veremos los datos de ella misma una foto de lo que hay en ese momento en la pagina, botones para acceder a la pagina dinamica, descargar la informacion en formato JSON o ir a la pagina para escribir comentarios, tambien una lista por paginas de 10 comentarios cada una de esa camara en orden descendiente de publicacion.
La pestaña de configuracion nos dara opciones para customizar la pagina, el nombre de usuario que aparecera en los comentarios, el tamaño de la letra y una fuente de letra. Tambien podremos cargar una configuracion a traves de un identificador de configuracion. 
La pestaña de ayuda da una breve descripcion de todo lo que hace la pagina web.
La pestaña de Admin veremos la pagina de administracion de Django para ver los modelos de nuestra aplicacion.
Por ultimo, el boton de cerrar sesion elimina los registros de las tablas de configuracion, comentarios, camaras. 

## Lista partes opcionales

* Inlcusión de un favicon del sitio
* Inclusion de boton para cerrar sesion
* crear una cache para el recuento de comentarios
