from django.shortcuts import render, get_object_or_404, redirect, reverse
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from .models import Camara, Comentario, Configuration
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
import random
import xml.etree.ElementTree as ET
from django.db.models import Count
from django.http import JsonResponse
from django.contrib import messages
from django.contrib.auth import logout

# Create your views here.
def get_session_settings(request):
    """Función para recuperar la configuración de sesion almacenada en las cookies del navegador"""
    font_select = request.COOKIES.get('font_select', 'var(--bs-body-font-family)')
    try:
        size_h1 = float(request.COOKIES.get('size_h1', 3))
        size_h2 = float(request.COOKIES.get('size_h2', 2))
        size_p = int(request.COOKIES.get('size_p', 16))
    except ValueError:
        size_h1 = 3
        size_h2 = 2
        size_p = 16
    return font_select, size_h1, size_h2, size_p

def pg_ayuda(request):
    """Maneja solicitudes a la pagina de ayuda del sitio web"""
    comentarios_count = Comentario.objects.count()
    camaras_count = Camara.objects.count()
    font_select, size_h1, size_h2, size_p = get_session_settings(request)
    return render(request, 'teveo/pg_ayuda.html', {
        'is_help_page': True,
        'comentarios_count': comentarios_count,
        'camaras_count': camaras_count,
        'font_select': font_select,
        'size_h1': size_h1,
        'size_h2': size_h2,
        'size_p': size_p
    })


def determine_size(size_option):
    """Determina el tamaño de los títulos y los párrafos."""
    sizes = {
        "Pequeña": (2, 1.5, 10),
        "Estándar": (3, 2, 16),
        "Grande": (4, 2.5, 28),
    }
    return sizes.get(size_option, (3, 2, 16))  # Default to "Estándar" if not found

def validate_session_id(request, session_id):
    try:
        return int(session_id)
    except ValueError:
        messages.error(request, 'El identificador debe ser un número entero.')
        return None

def get_configuration(request, session_id):
    try:
        return Configuration.objects.get(id=session_id)
    except Configuration.DoesNotExist:
        messages.warning(request, 'Este identificador no existe o es inválido.')
        return None

def get_default_settings_from_cookies(request):
    user_name = request.COOKIES.get('userName', 'Anónimo')
    font_type = request.COOKIES.get('font_select', 'var(--bs-body-font-family)')
    size_h1 = float(request.COOKIES.get('size_h1', 2.5))
    size_h2 = float(request.COOKIES.get('size_h2', 2))
    size_p = int(request.COOKIES.get('size_p', 18))
    return user_name, font_type, size_h1, size_h2, size_p

def update_settings_from_configuration(config):
    user_name = config.usuario
    font_type = config.tipoletra
    size_h1, size_h2, size_p = determine_size(config.tamano)  # Ensure determine_size is properly defined or replace with actual logic
    return user_name, font_type, size_h1, size_h2, size_p

def get_session_info(request, session_id):
    user_name, font_type, size_h1, size_h2, size_p = get_default_settings_from_cookies(request)
    session_cookie = None

    if session_id:
        validated_id = validate_session_id(request, session_id)
        if validated_id:
            config = get_configuration(request, validated_id)
            if config:
                user_name, font_type, size_h1, size_h2, size_p = update_settings_from_configuration(config)
                session_cookie = validated_id
                messages.success(request, 'Configuración cargada correctamente.')

    return user_name, font_type, size_h1, size_h2, size_p, session_cookie


def retrieve_configuration(request):
    """Crea la configuracion o Recupera la configuración guardada."""
    user_name = "Anónimo"  # voy a probar a poner "" sino dejar en "Anonimo"
    font_type = "var(--bs-body-font-family)"
    size_h1, size_h2, size_p = 3, 2, 16
    session_cookie = None

    if "configButton" in request.POST:
        user_name = request.POST.get('userName', '').strip() or "Anónimo"
        font_type = "var(--bs-body-font-family)" if "Sans-Serif" in request.POST.get('fontSelect', '') else request.POST.get('fontSelect', 'var(--bs-body-font-family)')
        size_h1, size_h2, size_p = determine_size(request.POST.get('sizeSelect', 'Estándar'))

        # Generate session ID
        session_id = random.randint(1, 100)
        while Configuration.objects.filter(id=session_id).exists():
            session_id = random.randint(1, 100)

        Configuration.objects.create(
            id=session_id,
            usuario=user_name,
            tipoletra=font_type,
            tamano=size_p
        )

        session_cookie = session_id
        messages.success(request, f'El ID de la sesión actual es {session_cookie}')

    elif "idRecovery" in request.POST and "idChecker" in request.POST:
        session_id = request.POST.get('idChecker', None)
        user_name, font_type, size_h1, size_h2, size_p, session_cookie = get_session_info(request, session_id)

    return user_name, font_type, size_h1, size_h2, size_p, session_cookie

def set_secure_cookie(response, key, value):
    response.set_cookie(key, value, httponly=True, secure=True)  # Use secure=True

@csrf_exempt
def configuracion(request):
    """Muestra y guarda la configuración."""
    comentarios_count = Comentario.objects.count()
    camaras_count = Camara.objects.count()

    if request.method == 'GET':
        user_name, font_type, size_h1, size_h2, size_p, session_cookie = get_session_info(request, request.COOKIES.get('id_session', None))
    else:
        user_name, font_type, size_h1, size_h2, size_p, session_cookie = retrieve_configuration(request)

    response = HttpResponse(render(request, 'teveo/pg_configuracion.html', {
        'is_configuration_page': True,
        'comentarios_count': comentarios_count,
        'camaras_count': camaras_count,
        'font_type': font_type,
        'size_h1': size_h1,
        'size_h2': size_h2,
        'size_p': size_p
    }))
    if session_cookie:
        set_secure_cookie(response, 'id_session', session_cookie)
    response.set_cookie('user_name', user_name)
    response.set_cookie('font_type', font_type)
    response.set_cookie('size_h1', size_h1)
    response.set_cookie('size_h2', size_h2)
    response.set_cookie('size_p', size_p)
    return response

def principal(request):
    comentarios = Comentario.objects.order_by('-fecha')
    paginator = Paginator(comentarios, 5)  # Paginar los comentarios, 5 por página
    page_number = request.GET.get('page', 1)  # Establece un valor por defecto en caso de no recibir 'page'
    try:
        page_obj = paginator.get_page(page_number)
    except PageNotAnInteger:
        page_obj = paginator.get_page(1)
    except EmptyPage:
        page_obj = paginator.get_page(paginator.num_pages)

    comentarios_count = Comentario.objects.count()
    camaras_count = Camara.objects.count()
    font_select, size_h1, size_h2, size_p = get_session_settings(request)
    context = {
        'page_obj': page_obj,
        'is_principal_page': True,
        'comentarios_count': comentarios_count,
        'camaras_count': camaras_count,
        'font_select': font_select,
        'size_h1': size_h1,
        'size_h2': size_h2,
        'size_p': size_p,
    }
    return render(request, 'teveo/principal.html', context)



def obtener_imagen_aleatoria():
    # Verificar si hay cámaras disponibles
    if Camara.objects.exists():
        # Seleccionar una cámara aleatoria de manera más eficiente
        camara_aleatoria = Camara.objects.order_by('?').first()
        return camara_aleatoria.url
    else:
        # Devolver una URL de imagen por defecto desde el directorio estático
        return 'images/no_image_available.png'


def process_xml_file(xml_file):
    """Procesa el archivo xml listao1, carga la informacion en la base de datos"""
    try:
        tree = ET.parse(xml_file)
        root = tree.getroot()
    except ET.ParseError as e:
        print(f"Error parsing XML: {e}")
        return
    except ValueError as e:
        print(f"Error with data conversion: {e}")
        return

    for camara_xml in root.findall('camara'):
        cam_id = camara_xml.find('id').text
        url = camara_xml.find('src').text
        lugar = camara_xml.find('lugar').text
        coordenadas = camara_xml.find('coordenadas').text.split(',')
        latitud = float(coordenadas[1])
        longitud = float(coordenadas[0])

        if not Camara.objects.filter(id=cam_id).exists():
            camara = Camara.objects.create(id=cam_id, descripcion="", url=url, lugar=lugar, latitud=latitud, longitud=longitud)
            camara.save()

def process_xml_file_listado2(xml_file):
    """Procesa el archivo xml listao2 y carga la informacion en la base de datos"""
    try:
        tree = ET.parse(xml_file)
        root = tree.getroot()
    except ET.ParseError as e:
        print(f"Error parsing XML: {e}")
        return
    except ValueError as e:
        print(f"Error with data conversion: {e}")
        return

    for cam_xml in root.findall('cam'):
        cam_id = cam_xml.get('id')
        url = cam_xml.find('url').text
        info = cam_xml.find('info').text
        latitud = float(cam_xml.find('place').find('latitude').text)
        longitud = float(cam_xml.find('place').find('longitude').text)

        if not Camara.objects.filter(id=cam_id).exists():
            camara = Camara.objects.create(id=cam_id, descripcion=info, url=url, lugar=info, latitud=latitud, longitud=longitud)
            camara.save()
def camaras(request):
    if request.method == 'POST' and 'descargar' in request.POST:
        try:
            process_xml_file('teveo/static/listxml/listado1.xml')
            process_xml_file_listado2('teveo/static/listxml/listado2.xml')
            messages.success(request, "XML files downloaded and database updated successfully.")
        except Exception as e:
            messages.error(request, f"Failed to process XML files: {e}")
            return HttpResponse("Failed to process XML files.", status=500)

    cameras = Camara.objects.annotate(num_comentarios=Count('comentario')).order_by('-num_comentarios')
    random_cam = obtener_imagen_aleatoria()
    is_cameras_page = True
    camaras_count = cameras.count()
    comentarios_count = Comentario.objects.count()
    font_select, size_h1, size_h2, size_p = get_session_settings(request)

    return render(request, 'teveo/camaras.html', {
        'cameras': cameras,
        'random_cam': random_cam,
        'is_cameras_page': is_cameras_page,
        'comentarios_count': comentarios_count,
        'camaras_count': camaras_count,
        'font_select': font_select,
        'size_h1': size_h1,
        'size_h2': size_h2,
        'size_p': size_p
    })


from django.shortcuts import get_object_or_404, render

def pg_camara(request, ident):
    camera_id = ident[:-4] if ident.endswith("-dyn") else ident
    camera = get_object_or_404(Camara, id=camera_id)

    comments = camera.comentario_set.order_by('-fecha').all()  # Usando _set para acceder a comentarios relacionados
    comentarios_count = Comentario.objects.count()
    camaras_count = Camara.objects.count()
    font_select, size_h1, size_h2, size_p = get_session_settings(request)

    context = {
        'camera': camera,
        'comments': comments,
        'comentarios_count': comentarios_count,
        'camaras_count': camaras_count,
        'font_select': font_select,
        'size_h1': size_h1,
        'size_h2': size_h2,
        'size_p': size_p
    }
    return render(request, 'teveo/pg_camara.html', context)




from django.core.cache import cache

def camara_info_json(request, ident):
    camera = get_object_or_404(Camara, id=ident)

    # Intentar obtener el número de comentarios desde la caché
    cache_key = f"camera_{camera.id}_comment_count"
    num_comentarios = cache.get(cache_key)

    if num_comentarios is None:
        num_comentarios = camera.comentario_set.count()
        cache.set(cache_key, num_comentarios, timeout=300)  # Caché válida por 5 minutos

    camara_info = {
        'id': camera.id,
        'descripcion': camera.descripcion,
        'url': camera.url,
        'lugar': camera.lugar,
        'latitud': camera.latitud,
        'longitud': camera.longitud,
        'numero_comentarios': num_comentarios,
    }

    return JsonResponse(camara_info)


import base64
import urllib.request
import logging

def download_image(url):
    """Descarga una imagen de la URL dada y la devuelve como bytes"""
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:100.0) Gecko/20100101 Firefox/100.0'
    }
    request = urllib.request.Request(url=url, headers=headers)
    try:
        with urllib.request.urlopen(request, timeout=10) as response:  # Tiempo de espera de 10 segundos
            image = response.read()
    except urllib.error.URLError as e:
        logging.error(f"Error al descargar la imagen desde {url}: {e}")
        return None
    except urllib.error.HTTPError as e:
        logging.error(f"Error HTTP al descargar la imagen desde {url}: {e}")
        return None
    return image


from django.core.paginator import Paginator

def camara_dyn(request, ident):
    camera = get_object_or_404(Camara, id=ident)
    comentarios_list = Comentario.objects.filter(camara=camera).order_by('-fecha')
    paginator = Paginator(comentarios_list, 10)  # 10 comentarios por página
    page_number = request.GET.get('page')
    comentarios = paginator.get_page(page_number)

    comentarios_count = Comentario.objects.filter(camara=camera).count()
    camaras_count = Camara.objects.count()
    font_select, size_h1, size_h2, size_p = get_session_settings(request)

    context = {
        'camera': camera,
        'comentarios': comentarios,
        'comentarios_count': comentarios_count,
        'camaras_count': camaras_count,
        'font_select': font_select,
        'size_h1': size_h1,
        'size_h2': size_h2,
        'size_p': size_p
    }
    return render(request, 'teveo/camaras_dyn.html', context)


import mimetypes


def image_embedded(request, ident):
    camera = get_object_or_404(Camara, id=ident)
    image = download_image(camera.url)
    if image is None:
        return HttpResponse("Error al descargar la imagen", status=500)

    # Detectar el tipo MIME de la imagen basado en la URL
    mime_type, _ = mimetypes.guess_type(camera.url)
    if mime_type is None:
        mime_type = "image/jpeg"  # Asumir JPEG si no se puede determinar

    image_base64 = base64.b64encode(image).decode('utf-8')
    html = f'<img id="img-cam" class="mx-auto d-block" src="data:{mime_type};base64,{image_base64}" alt="">'
    return HttpResponse(html, content_type="text/html")


from django.template.loader import render_to_string

def comentarios_htmx(request, ident):
    camera = get_object_or_404(Camara, id=ident)
    comentarios = Comentario.objects.filter(camara=camera).order_by('-fecha')

    # Renderizar los comentarios utilizando una plantilla
    comentarios_html = render_to_string('teveo/comentarios_template.html', {'comentarios': comentarios})
    return HttpResponse(comentarios_html, content_type="text/html")


def comentario(request):
    if request.method == 'POST':
        camera_id = request.POST.get('camera_id')
        camera = get_object_or_404(Camara, id=camera_id)
        comentario_texto = request.POST.get('comment_text')

        # Obtener la información de la sesión del usuario
        user_name = "Anónimo"
        session_cookie = request.COOKIES.get('id_session', None)
        if session_cookie:
            try:
                session = Configuration.objects.get(id=session_cookie)
                user_name = session.usuario
            except Configuration.DoesNotExist:
                pass

        Comentario.objects.create(
            camara=camera,
            contenido=comentario_texto,
            nombre=user_name,
        )

        return redirect(reverse('comentario') + '?camera_id=' + camera_id)

    camera_id = request.GET.get('camera_id')
    camera = get_object_or_404(Camara, id=camera_id)
    comentarios_count = Comentario.objects.count()
    comentarios = Comentario.objects.all()
    camaras_count = Camara.objects.count()
    font_select, size_h1, size_h2, size_p = get_session_settings(request)

    return render(request, 'teveo/comentario.html', {
        'camera': camera,
        'comentarios': comentarios,
        'comentarios_count': comentarios_count,
        'camaras_count': camaras_count,
        'font_select': font_select,
        'size_h1': size_h1,
        'size_h2': size_h2,
        'size_p': size_p
    })

def cerrar_sesion(request):
    Configuration.objects.all().delete()
    Comentario.objects.all().delete()
    Camara.objects.all().delete()
    logout(request)
    request.session.flush()
    return redirect('/')
