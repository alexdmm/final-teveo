from django.test import TestCase, Client
from django.urls import reverse
from .models import Camara, Comentario, Configuration

# Create your tests here.

class BaseparaTests(TestCase):
    def setUp(self):
        self.client = Client()
        self.camara = Camara.objects.create(
            id="cam-001",
            descripcion="Descripción de la cámara",
            url="http://example.com",
            lugar="Lugar de la cámara",
            latitud=10.0,
            longitud=20.0
        )
        self.comentario = Comentario.objects.create(
            camara=self.camara,
            contenido="Contenido del comentario",
            nombre="Usuario"
        )

class PgAyudaViewTests(BaseparaTests):
    def test_pg_ayuda_view(self):
        response = self.client.get(reverse('pg_ayuda'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teveo/pg_ayuda.html')
        self.assertTrue('is_help_page' in response.context)
        self.assertTrue(response.context['is_help_page'])
        self.assertEqual(response.context['comentarios_count'], 1)
        self.assertEqual(response.context['camaras_count'], 1)
        self.assertIn('font_select', response.context)
        self.assertIn('size_h1', response.context)
        self.assertIn('size_h2', response.context)
        self.assertIn('size_p', response.context)

class PrincipalViewTests(BaseparaTests):
    def test_principal_view(self):
        response = self.client.get(reverse('principal'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teveo/principal.html')
        self.assertTrue('is_principal_page' in response.context)
        self.assertTrue(response.context['is_principal_page'])
        self.assertTrue('page_obj' in response.context)
        self.assertTrue('comentarios_count' in response.context)
        self.assertTrue('camaras_count' in response.context)
        self.assertTrue('font_select' in response.context)
        self.assertTrue('size_h1' in response.context)
        self.assertTrue('size_h2' in response.context)
        self.assertTrue('size_p' in response.context)



class ConfiguracionViewTests(TestCase):
    def setUp(self):
        self.client = Client()

    def test_configuracion_view_get(self):
        # Preparar datos de configuración
        Configuration.objects.create(id=1, usuario="Usuario1", tipoletra="Arial", tamano="Pequeña")

        # Realizar la solicitud GET a la vista de configuración
        response = self.client.get(reverse('configuracion'))

        # Verificar el código de estado de la respuesta y la plantilla utilizada
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teveo/pg_configuracion.html')

        # Verificar que los datos de configuración se pasan al contexto correctamente
        self.assertTrue('is_configuration_page' in response.context)
        self.assertTrue(response.context['is_configuration_page'])
        self.assertTrue('comentarios_count' in response.context)
        self.assertTrue('camaras_count' in response.context)
        self.assertTrue('font_type' in response.context)
        self.assertEqual(response.context['font_type'], 'var(--bs-body-font-family)')
        self.assertTrue('size_h1' in response.context)
        self.assertTrue('size_h2' in response.context)
        self.assertTrue('size_p' in response.context)

    def test_configuracion_view_post(self):
        # Realizar la solicitud POST a la vista de configuración
        response = self.client.post(reverse('configuracion'), data={
            'configButton': 'Save',
            'userName': 'Usuario2',
            'fontSelect': 'Arial',
            'sizeSelect': 'Pequeña'
        })

        # Verificar el código de estado de la respuesta y la redirección
        self.assertEqual(response.status_code, 200)

        # Verificar que se ha creado una nueva configuración
        configuration = Configuration.objects.last()
        self.assertEqual(configuration.usuario, 'Usuario2')
        self.assertEqual(configuration.tipoletra, 'Arial')
        self.assertEqual(configuration.tamano, '10') #aquí en realidad es Pequeña lo que debería poner

class CamarasViewTests(BaseparaTests):

    def test_camaras_view(self):
        response = self.client.get(reverse('camaras'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teveo/camaras.html')
        self.assertTrue('cameras' in response.context)
        self.assertTrue('random_cam' in response.context)
        self.assertTrue('is_cameras_page' in response.context)
        self.assertTrue(response.context['is_cameras_page'])
        self.assertTrue('comentarios_count' in response.context)
        self.assertTrue('camaras_count' in response.context)
        self.assertTrue('font_select' in response.context)
        self.assertTrue('size_h1' in response.context)
        self.assertTrue('size_h2' in response.context)
        self.assertTrue('size_p' in response.context)

class PgCamaraViewTests(BaseparaTests):

    def test_pg_camara_view_existing(self):
        response = self.client.get(reverse('pg_camara', args=['cam-001']))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teveo/pg_camara.html')
        self.assertTrue('camera' in response.context)
        self.assertTrue('comments' in response.context)
        self.assertTrue('comentarios_count' in response.context)
        self.assertTrue('camaras_count' in response.context)
        self.assertTrue('font_select' in response.context)
        self.assertTrue('size_h1' in response.context)
        self.assertTrue('size_h2' in response.context)
        self.assertTrue('size_p' in response.context)

    def test_pg_camara_view_non_existing(self):
        response = self.client.get(reverse('pg_camara', args=['non-existent-id']))
        self.assertEqual(response.status_code, 404)

class CamaraInfoJsonViewTests(BaseparaTests):
    def test_camara_info_json_view_existing(self):
        response = self.client.get(reverse('camara_info_json', args=['cam-001']))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['content-type'], 'application/json')
        camara_info = response.json()
        self.assertEqual(camara_info['id'], 'cam-001')
        self.assertEqual(camara_info['descripcion'], 'Descripción de la cámara')
        self.assertEqual(camara_info['url'], 'http://example.com')
        self.assertEqual(camara_info['lugar'], 'Lugar de la cámara')
        self.assertEqual(camara_info['latitud'], 10.0)
        self.assertEqual(camara_info['longitud'], 20.0)
        self.assertEqual(camara_info['numero_comentarios'], 1)

    def test_camara_info_json_view_non_existing(self):
        response = self.client.get(reverse('camara_info_json', args=['non-existent-id']))
        self.assertEqual(response.status_code, 404)

class CamaraDynViewTests(BaseparaTests):

    def test_camara_dyn_view_existing(self):
        response = self.client.get(reverse('camara_dyn', args=['cam-001']))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teveo/camara_dyn.html')
        self.assertIn('camera', response.context)
        self.assertIn('comentarios', response.context)
        self.assertIn('comentarios_count', response.context)
        self.assertIn('camaras_count', response.context)
        self.assertIn('font_select', response.context)
        self.assertIn('size_h1', response.context)
        self.assertIn('size_h2', response.context)
        self.assertIn('size_p', response.context)

    def test_camara_dyn_view_non_existing(self):
        response = self.client.get(reverse('camara_dyn', args=['non-existent-id']))
        self.assertEqual(response.status_code, 404)

class ComentarioViewTests(BaseparaTests):

    def test_comentario_view_post(self):
        post_data = {
            'camera_id': 'cam-001',
            'comment_text': 'Contenido del comentario',
        }
        response = self.client.post(reverse('comentario'), data=post_data)
        self.assertEqual(response.status_code, 302)  # Verificar redirección después de crear el comentario

        # Verificar si el comentario se creó correctamente
        comentario = Comentario.objects.filter(camara=self.camara, contenido='Contenido del comentario').first()
        self.assertIsNotNone(comentario)
        self.assertEqual(comentario.nombre, 'Usuario')  # El Usuario por defecto se llamará Usuario

    def test_comentario_view_get(self):
        response = self.client.get(reverse('comentario') + '?camera_id=cam-001')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teveo/comentario.html')
