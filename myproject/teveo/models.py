

from django.db import models
# Create your models here.


class Camara(models.Model):
    id = models.CharField(max_length=100, primary_key=True)  # id de la camara
    descripcion = models.TextField()
    url = models.URLField()
    lugar = models.CharField(max_length=100)  # nombre del lugar de la camara
    latitud = models.FloatField()
    longitud = models.FloatField()

    def __str__(self):
        return self.id


class Comentario(models.Model):
    nombre = models.CharField(max_length=50, default="Anónimo")  # nombre del usuario
    camara = models.ForeignKey(Camara, on_delete=models.CASCADE)
    contenido = models.TextField()
    fecha = models. DateTimeField(auto_now_add=True)


class Configuration(models.Model):
    id = models.IntegerField(primary_key=True)
    usuario = models.CharField(max_length=50)
    tamano = models.CharField(max_length=50)
    tipoletra = models.CharField(max_length=50)

    def __str__(self):
        return str(self.usuario)

