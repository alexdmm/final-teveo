

from django.urls import path
from . import views

urlpatterns = [
    path("", views.principal, name="principal"),
    path("ayuda/", views.pg_ayuda, name="pg_ayuda"),
    path("configuracion/", views.configuracion, name="configuracion"),
    path("camaras/", views.camaras, name="camaras"),
    path("camaras/<str:ident>/", views.pg_camara, name='pg_camara'),
    path("camaras/<str:ident>/json/", views.camara_info_json, name='camara_info_json'),
    path("camaras/<str:ident>/-dyn/", views.camara_dyn, name='camara_dyn'),
    path("teveo/htmx/image/<str:ident>/", views.image_embedded, name='image_embedded'),
    path('teveo/htmx/comentarios/<str:ident>/', views.comentarios_htmx, name='comentarios_htmx'),
    path("comentario/", views.comentario, name='comentario'),
    path("logout/", views.cerrar_sesion, name="cerrar_sesion"),
]

